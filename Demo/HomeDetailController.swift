//
//  HomeDetailController.swift
//  Demo
//
//  Created by nhatnt on 05/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class HomeDetailController: UIViewController {

    @IBAction func dismiss(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
