//
//  AboutCell.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class AboutCell: BaseCell {

    
    @IBOutlet weak var aboutLabel: UILabel!
    //MARK: Variables
    override var item: ProfileViewModelItem? {
        didSet{
            guard let item = item as? ProfileViewModelAboutItem else {
                return
            }
            self.aboutLabel.text = item.about
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
