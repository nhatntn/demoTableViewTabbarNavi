//
//  AttributeCell.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class AttributeCell: UITableViewCell {

    @IBOutlet weak var AttributeNameLabel: UILabel!
    @IBOutlet weak var AttributeInfoLabel: UILabel!
    //MARK: Variables
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    var item : Attribute? {
        didSet{
            guard let item = item else {
                return
            }
            AttributeNameLabel.text = item.key
            AttributeInfoLabel.text = item.value
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
