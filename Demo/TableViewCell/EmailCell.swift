//
//  EmailCell.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class EmailCell: BaseCell {

    @IBOutlet weak var emailLabel: UILabel!
    //MARK: Variables
    override var item: ProfileViewModelItem? {
        didSet{
            guard let item = item as? ProfileViewModelEmailItem else {
                return
            }
            emailLabel.text = item.email
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
