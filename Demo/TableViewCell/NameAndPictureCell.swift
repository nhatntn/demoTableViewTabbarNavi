//
//  NameAndPictureCell.swift
//  Demo
//
//  Created by nhatnt on 04/10/2017.
//  Copyright © 2017 nhatnt. All rights reserved.
//

import UIKit

class NameAndPictureCell: BaseCell {

    //MARK: UI Elements
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    //MARK: Variables
    override var item: ProfileViewModelItem? {
        didSet{
            guard let item = item as? ProfileViewModelNamePictureItem else {
                return
            }
            
            profileName.text = item.name
            profileImage.image = UIImage(named: item.pictureUrl)
        }
    }
    
    //MARK: Initialize
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileImage?.layer.cornerRadius = 52
        profileImage?.clipsToBounds = true
        profileImage?.contentMode = .scaleAspectFit
        profileImage?.backgroundColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
